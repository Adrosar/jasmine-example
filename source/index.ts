export class Boo {

    private _name$1: string;

    constructor(name: string) {
        this._name$1 = name;
    }

    public getName(): string {
        return this._name$1;
    }
}

# jasmine-example

Repo zostało stworzone jako przykład wykorzystania testów [Jasmine](https://github.com/jasmine/jasmine) w środowisku przeglądarki internetowej. Dodatkowo zostało zbudowane na bazie [ts-startek-kit](https://bitbucket.org/Adrosar/ts-startek-kit/src/2.0.0/) dzięki czemu aplikację i testy można pisać w TypeScript, a następnie używając [Rollup](https://rollupjs.org) lub [Browserify](http://browserify.org) zbudować jeden plik **JS**.

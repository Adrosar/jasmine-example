#!/usr/bin/env node

const express = require('express');
const serveIndex = require('serve-index');
const { projectResolve } = require('./helpers');

const staticOptions = {
    etag: true,
    maxAge: 60
};

function startServer(_port, _hostname) {
    if (!(typeof _hostname === 'string' && _hostname.length > 0)) {
        _hostname = '0.0.0.0';
    }

    const app = express();

    app.use('/~app.browserify.js', function (_req, _res, _next) {
        _res.sendFile(projectResolve('build/app.browserify.js'));
    });

    app.use('/~test.browserify.js', function (_req, _res, _next) {
        _res.sendFile(projectResolve('build/test.browserify.js'));
    });

    app.use('/~app.rollup.js', function (_req, _res, _next) {
        _res.sendFile(projectResolve('build/app.rollup.js'));
    });

    app.use('/~test.rollup.js', function (_req, _res, _next) {
        _res.sendFile(projectResolve('build/test.rollup.js'));
    });

    app.use('/~assets', express.static(projectResolve('assets'), staticOptions));
    app.use('/~typedoc', express.static(projectResolve('typedoc'), staticOptions));

    app.use('/', express.static(projectResolve('web'), staticOptions));
    app.use('/', serveIndex(projectResolve('web'), {
        'icons': true
    }));

    app.listen(_port, _hostname, function () {
        console.log('Server listening → ' + _hostname + ':' + _port + ' !');
    })
}

function main() {
    const argv = process.argv || [];

    let port = 0;
    let hostname = '';

    for (let i = 0; i < argv.length; i++) {
        const first = argv[i];
        const second = argv[i + 1];

        if (first === '-p' || first === '--port') {
            if (second && typeof second === 'string') {
                port = parseInt(second);
            }
        }

        if (first === '-h' || first === '--host') {
            if (second && typeof second === 'string') {
                hostname = second;
            }
        }
    }

    if (port) {
        startServer(port, hostname);
    }
}

main();
import { Boo } from "../source/index";

describe('Boo test', function () {
    const boo = new Boo("boo");

    it('"boo" powinno być obiektem', function () {
        expect(typeof boo).toBe('object');
    });

    it('Metoda "getName" powinna być funkcją', function () {
        expect(typeof boo.getName).toBe('function');
    });

    it('Wywołanie metody "getName" powinna zwrócić tekst "Boo"', function () {
        expect(boo.getName()).toBe('boo');
    });
});